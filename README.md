AutoClicker - For Windows Systems

This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/. This software comes without any warranty, express or implied. You assume all liability using this software. Some of these files may be licensed under different licenses.

To use:

In order to compile or run the python script, ensure Python 2.7 is installed. An executable has been included for convienience.

Either compile the application and run the executable, or run the python script without compiling.

Once the script or application is running, pressing the left CTRL key on your keyboard will cause the mouse to rapidly left click. The application can be left to run in the background. Simply close the application window when done using.

This can be useful for games such as Minecraft.

Questions or comments can be directed to John Minton <cjohnweb@gmail.com>

Enjoy!